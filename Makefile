MAKEFLAGS += --no-builtin-rules --no-builtin-variables

.SUFFIXES:

.SECONDARY:

.SECONDEXPANSION:

CREDIBLE := \
  $(addprefix build/credible/Re_λλ-Im_λλ----, $(addsuffix .pdf, \
    Re_λλ-Im_λλ--R_K \
    Re_λλ-Im_λλ--Δm \
    Re_λλ-Im_λλ--ϕ_12 \
    Re_λλ-Im_λλ--Δm---Re_λλ-Im_λλ--ϕ_12 \
    Re_λλ-Im_λλ--Δm-ϕ_12 \
    Re_λλ-Im_λλ--Δm---Re_λλ-Im_λλ--ϕ_12---Re_λλ-Im_λλ--Δm-ϕ_12 \
    Re_λλ-Im_λλ--R_K---Re_λλ-Im_λλ--Δm-ϕ_12 \
    Re_λλ-Im_λλ--R_K---Re_λλ-Im_λλ--Δm-ϕ_12---Re_λλ-Im_λλ--Δm-ϕ_12-R_K \
    Re_λλ-Im_λλ--Δm-ϕ_12-R_K \
  ))

TEXCMD := tex-utils/tex.sh --tex-inputs "$(shell pwd)/header-components/" --biber

build/talk.pdf: talk.tex header/talk.tex header/talk-packages.tex lit.bib content/*.tex $(CREDIBLE) | build
	@$(TEXCMD) talk.tex

build/samples/%.hdf5: run/%.sh | build/samples build/log
	$<

build/histogram/%.pkl: script/histogram.py build/samples/$$(lastword $$(subst ---, , $$*)).hdf5 | build/histogram
	python3 $?

build/credible/%.pdf: script/plot.py \
  $$(addprefix build/histogram/, \
    $$(addsuffix .pkl, \
      $$(addprefix $$(firstword $$(subst ----, , $$*))---, \
        $$(subst ---, , $$(lastword $$(subst ----, , $$*))) \
      ) \
    ) \
  ) \
  | build/credible
	python3 $< $@

build $(addprefix build/, samples log histogram credible):
	mkdir -p $@

fast:
	@$(TEXCMD) --fast talk.tex

clean:
	rm -rf build

.PHONY: clean fast
