#!/usr/bin/env python3

import pickle
import sys

import numpy as np
import scipy.special

import matplotlib as mpl
import matplotlib.pyplot as plt

import seaborn as sns
sns.reset_orig()

from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf)

param_aspect_equal = [
    ('Re_λλ', 'Im_λλ')
]

param_labels = {
    'M': r"$M \mathbin{/} \si{\giga\electronvolt}$",
    'Re_λλ': r"$\Re \lambda_{\mathrm{b e}}^* \lambda^{}_{\mathrm{s e}}$",
    'Im_λλ': r"$\Im \lambda_{\mathrm{b e}}^* \lambda^{}_{\mathrm{s e}}$",
}

def region_heights(data, levels):
    levels = scipy.special.erf(np.array(levels) / np.sqrt(2))
    sorted_data = np.sort(data, axis=None)[::-1]
    total = np.sum(sorted_data)
    current = sorted_data[0]
    indexes = np.zeros(len(levels), int)
    for i in range(len(levels)):
        if i > 0:
            indexes[i] = indexes[i - 1]
        while current < levels[i] * total:
            indexes[i] += 1
            current += sorted_data[indexes[i]]
    return sorted_data[indexes]

def region_norm(data, levels):
    return mpl.colors.BoundaryNorm([0] + list(region_heights(data, levels))[::-1] + [np.inf], len(levels) + 1)

def plot_with_marginals(ax, ax_x, ax_y, data, bins, ranges, levels=None, cmap='Blues'):
    bins_x = bins[0]
    bins_y = bins[1]
    data_x = np.sum(data, axis=0)
    data_x /= np.max(data_x)
    data_y = np.sum(data, axis=1)
    data_y /= np.max(data_y)

    norm = None
    if levels is not None:
        norm = region_norm(data, levels)
    ax.pcolor(bins_x, bins_y, data, cmap=cmap, norm=norm)

    im = mpl.image.imread('graphic/arxiv.png')
    imagebox = mpl.offsetbox.OffsetImage(im, zoom=0.2, interpolation='none')
    ax.add_artist(mpl.offsetbox.AnnotationBbox(imagebox, (0, 0), pad=0, frameon=False))

    #ax.plot([0], [0], 'xw', markersize=12, markeredgecolor='w', markeredgewidth=7)
    #ax.plot([0], [0], 'xk', markersize=8,  markeredgecolor='k', markeredgewidth=3)

    #ax.plot([0], [0], 'ok')

    ax_x.plot(bins_x[:-1], data_x, drawstyle='steps-post', color='k')
    if levels is not None:
        norm = region_norm(data_x, levels)
        ax_x.bar(bins_x[:-1], data_x, width=np.ediff1d(bins_x), color=mpl.cm.ScalarMappable(norm, cmap).to_rgba(data_x), linewidth=0)

    ax_y.plot(data_y, bins_y[:-1], drawstyle='steps-pre', color='k')
    if levels is not None:
        norm = region_norm(data_y, levels)
        ax_y.barh(bins_y[:-1], data_y, height=np.ediff1d(bins_y), color=mpl.cm.ScalarMappable(norm, cmap).to_rgba(data_y), linewidth=0)

def setup_figure():
    fig = plt.figure()
    gs = mpl.gridspec.GridSpec(2, 2, width_ratios=[5, 1], height_ratios=[1, 5])

    ax = fig.add_subplot(gs[2])

    ax_x = fig.add_subplot(gs[0], sharex=ax)
    ax_y = fig.add_subplot(gs[3], sharey=ax)

    return fig, ax, ax_x, ax_y

def finalise_figure(fig, ax, ax_x, ax_y):
    ax.grid()

    plt.setp(ax_x.get_xticklabels(), visible=False)
    plt.setp(ax_x.get_yticklabels(), visible=False)
    plt.setp(ax_x.xaxis.get_ticklines(), visible=False)
    plt.setp(ax_x.yaxis.get_ticklines(), visible=False)
    plt.setp(ax_y.get_xticklabels(), visible=False)
    plt.setp(ax_y.get_yticklabels(), visible=False)
    plt.setp(ax_y.xaxis.get_ticklines(), visible=False)
    plt.setp(ax_y.yaxis.get_ticklines(), visible=False)

    sns.utils.despine(ax=ax_x, left=True)
    sns.utils.despine(ax=ax_y, bottom=True)
    fig.tight_layout()
    fig.subplots_adjust(hspace=0, wspace=0)

def color_alpha(color, alpha):
    return mpl.colors.colorConverter.to_rgba(color, alpha)

filename = sys.argv[1].rsplit('.', 1)[0].rsplit('/', 1)[-1]
params = filename.split('----', 1)[0].split('-')
constraints = filename.split('----', 1)[-1].split('---')

if len(constraints) == 1:
    levels = [1, 2, 3]
    colors = [['white', 'green', 'blue', 'red']]
else:
    levels = [2]
    colors = [[color_alpha('white', 0), color_alpha(c, 0.5)] for c in ['blue', 'red', 'green', 'brown', 'yellow']]

fig, ax, ax_x, ax_y = setup_figure()

for i, constraint in enumerate(constraints):
    with open('build/histogram/{}---{}.pkl'.format('-'.join(params), constraint), 'rb') as f:
        hist, bins, ranges = pickle.load(f)
    plot_with_marginals(ax, ax_x, ax_y, hist, bins, ranges, levels=levels, cmap=mpl.colors.ListedColormap(colors[i]))

finalise_figure(fig, ax, ax_x, ax_y)

if tuple(params) in param_aspect_equal:
    ax.set_aspect('equal')
ax.set_xlabel(param_labels[params[0]])
ax.set_ylabel(param_labels[params[1]])

fig.savefig(sys.argv[1])
