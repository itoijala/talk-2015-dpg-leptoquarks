#!/bin/bash

file="$(basename "$0")"
file="${file%.*}"

args=(
	--output "build/samples/${file}.hdf5"
	--store-observables-and-proposals
	--store-prerun

	--chains 4
	--chunk-size 100000
	--chunks 100
	--prerun-update 100000
	--prerun-min 2000000
	--prerun-max 2000000

	--global-option model Leptoquark
	--global-option scan-mode cartesian
	--global-option lq-lepton e

	--fix "M_LQ" 1e3
	--scan "Re{ll_LQ}" -1e0 1e0 --prior flat
	--scan "Im{ll_LQ}" -1e0 1e0 --prior flat
#	--scan "Abs{ll_LQ}" 0 1e0 --prior flat
#	--scan "Arg{ll_LQ}" 0 6.3 --prior flat

	--constraint "B-mixing::Delta_m_s@HFAG-2014"
	--constraint "B-mixing::phi_12_s@HFAG-2014"

#	--nuisance "GSW::sin^2(theta)"     0.2309      0.2314    --prior gaussian    0.231       0.2312      0.2313
#	--nuisance "CKM::A"                0.762       0.846     --prior log-gamma   0.786       0.81        0.828
#	--nuisance "CKM::lambda"           0.2248      0.2268    --prior log-gamma   0.2251      0.2255      0.2262
#	--nuisance "CKM::rhobar"           0.1307      0.1719    --prior log-gamma   0.138       0.1453      0.1586
#	--nuisance "CKM::etabar"           0.319       0.365     --prior log-gamma   0.331       0.343       0.354
#	--nuisance "QCD::alpha_s(MZ)"      0.1170      0.1198    --prior gaussian    0.1177      0.1184      0.1191
#	--nuisance "mass::c"               1.225       1.325     --prior gaussian    1.25        1.275       1.3
#	--nuisance "mass::b(MSbar)"        4.12        4.24      --prior gaussian    4.15        4.18        4.21
#	--nuisance "mass::t(pole)"       171.5       175.5       --prior gaussian  172.5       173.5       174.5
	--nuisance "decay-constant::B_s"   0.2176      0.2376    --prior gaussian    0.2226      0.2276      0.2326
	--nuisance "bag-parameter::B_s"    1.21        1.45      --prior gaussian    1.27        1.33        1.39

#	--nuisance "hbar"                  6.582e-25   6.582e-25 --prior gaussian    6.582e-25   6.582e-25   6.582e-25
#	--nuisance "mu"                    0.6        15         --prior log-gamma   2.4         4.2         9.6
#	--nuisance "QCD::mu_t"             MIN  MAX              --prior flat
#	--nuisance "QCD::mu_b"             MIN  MAX              --prior flat
#	--nuisance "QCD::mu_c"             MIN  MAX              --prior flat
#	--nuisance "QCD::Lambda"           MIN  MAX              --prior flat
#	--nuisance "G_Fermi"               1.166e-05   1.166e-05 --prior gaussian    1.166e-05   1.166e-05   1.166e-05
#	--nuisance "mass::ud(2GeV)"        MIN  MAX              --prior flat
#	--nuisance "mass::s(2GeV)"         0.085      -0.075     --prior log-gamma   0.09        0.095       0.01
#	--nuisance "mass::B_s"             5.366       5.367     --prior gaussian    5.367       5.367       5.367
#	--nuisance "mass::W"              80.36       80.42      --prior gaussian   80.37       80.39       80.4
#	--nuisance "mass::Z"              91.18       91.19      --prior gaussian   91.19       91.19       91.19
#	--nuisance "b->s::mu_0c"           MIN  MAX              --prior flat
#	--nuisance "b->s::mu_0t"           MIN  MAX              --prior flat
)

eos/src/clients/eos-scan-mc "${args[@]}" "$@" 2>&1 | tee "build/log/${file}.log"
