#!/bin/bash

file="$(basename "$0")"
file="${file%.*}"

args=(
	--output "build/samples/${file}.hdf5"
	--store-observables-and-proposals
	--store-prerun

	--chains 4
	--chunk-size 1000
	--chunks 1000
	--prerun-update 400
	--prerun-min    400
	--prerun-max 100000

	--global-option model Leptoquark
	--global-option scan-mode cartesian
	--global-option lq-lepton e

	--fix "M_LQ" 1e3
	--scan "Re{ll_LQ}" -1e0 1e0 --prior flat
	--scan "Im{ll_LQ}" -1e0 1e0 --prior flat

	--constraint "B^+->K^+l^+l^-::R_K[1.00,6.00]@LHCb-2014"

#	--nuisance "GSW::sin^2(theta)"                     0.2309      0.2314    --prior gaussian     0.231        0.2312       0.2313
#	--nuisance "CKM::A"                                0.762       0.846     --prior log-gamma    0.786        0.81         0.828
#	--nuisance "CKM::lambda"                           0.2248      0.2268    --prior log-gamma    0.2251       0.2255       0.2262
#	--nuisance "CKM::rhobar"                           0.1307      0.1719    --prior log-gamma    0.138        0.1453       0.1586
#	--nuisance "CKM::etabar"                           0.319       0.365     --prior log-gamma    0.331        0.343        0.354
#	--nuisance "QED::alpha_e(m_b)"                     0.007519    0.008106  --prior log-gamma    0.007519     0.007519     0.007812
#	--nuisance "QCD::alpha_s(MZ)"                      0.1170      0.1198    --prior gaussian     0.1177       0.1184       0.1191
#	--nuisance "mass::c"                               1.225       1.325     --prior gaussian     1.25         1.275        1.3
#	--nuisance "mass::b(MSbar)"                        4.12        4.24      --prior gaussian     4.15         4.18         4.21
#	--nuisance "mass::t(pole)"                       171.5       175.5       --prior gaussian   172.5        173.5        174.5
#	--nuisance "decay-constant::B_u"                   0.1812      0.2       --prior gaussian     0.1859       0.1906       0.1953
#	--nuisance "decay-constant::K_u"                   0.1539      0.1583    --prior gaussian     0.155        0.1561       0.1572
#	--nuisance "life_time::B_u"                        1.625e-12   1.657e-12 --prior gaussian     1.633e-12    1.641e-12    1.649e-12
#	--nuisance "B->K::F^p(0)@KMPW2010"                 0.3         0.44      --prior log-gamma    0.32         0.34         0.39
#	--nuisance "B->K::F^t(0)@KMPW2010"                 0.33        0.49      --prior log-gamma    0.36         0.39         0.44
#	--nuisance "B->K::b^p_1@KMPW2010"                 -5.3        -0.3       --prior log-gamma   -3.7         -2.1         -1.2
#	--nuisance "B->K::b^0_1@KMPW2010"                 -6.1        -2.7       --prior log-gamma   -5.2         -4.3         -3.5
#	--nuisance "B->K::b^t_1@KMPW2010"                 -6.2        -0.2       --prior log-gamma   -4.2         -2.2         -1.2
#	--nuisance "lambda_B_p"                            0.255       0.715     --prior gaussian     0.37         0.485        0.6
#	--nuisance "B->K::a_1@1GeV"                        0           0.12      --prior gaussian     0.03         0.06         0.09
#	--nuisance "B->K::a_2@1GeV"                       -0.05        0.55      --prior gaussian     0.1          0.25         0.4
#	--nuisance "B->Pll::Lambda_pseudo@LargeRecoil"    -1           1         --prior gaussian    -0.5          0            0.5
#	--nuisance "B->Pll::sl_phase_pseudo@LargeRecoil"  -3.142       3.142     --prior gaussian    -1.571        0            1.571

#	--nuisance "hbar"                                  6.582e-25   6.582e-25 --prior gaussian     6.582e-25    6.582e-25    6.582e-25
#	--nuisance "mu"                                    0.6         15        --prior log-gamma    2.4          4.2          9.6
#	--nuisance "QCD::mu_t"                             MIN         MAX       --prior flat
#	--nuisance "QCD::mu_b"                             MIN         MAX       --prior flat
#	--nuisance "QCD::mu_c"                             MIN         MAX       --prior flat
#	--nuisance "QCD::Lambda"                           MIN         MAX       --prior flat
#	--nuisance "G_Fermi"                               1.166e-05   1.166e-05 --prior gaussian     1.166e-05    1.166e-05    1.166e-05
#	--nuisance "mass::mu"                              MIN         MAX       --prior flat
#	--nuisance "mass::ud(2GeV)"                        MIN         MAX       --prior flat
#	--nuisance "mass::s(2GeV)"                         0.085       -0.075    --prior log-gamma    0.09         0.095        0.01
#	--nuisance "mass::K_u"                             0.4936      0.4937    --prior gaussian     0.4937       0.4937       0.4937
#	--nuisance "mass::B_u"                             5.279       5.28      --prior gaussian     5.279        5.279        5.279
#	--nuisance "mass::W"                              80.36       80.42      --prior gaussian    80.37        80.39        80.4
#	--nuisance "mass::Z"                              91.18       91.19      --prior gaussian    91.19        91.19        91.19
#	--nuisance "b->s::mu_0c"                           MIN         MAX       --prior flat
#	--nuisance "b->s::mu_0t"                           MIN         MAX       --prior flat
)

eos/src/clients/eos-scan-mc "${args[@]}" "$@" 2>&1 | tee "build/log/${file}.log"
