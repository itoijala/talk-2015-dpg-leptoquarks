/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2011, 2013 Danny van Dyk
 * Copyright (c) 2014 Frederik Beaujean
 * Copyright (c) 2014 Christoph Bobeth
 *
 * This file is part of the EOS project. EOS is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * EOS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef EOS_GUARD_SRC_UTILS_WILSON_SCAN_MODEL_HH
#define EOS_GUARD_SRC_UTILS_WILSON_SCAN_MODEL_HH 1

#include <eos/utils/model.hh>
#include <eos/utils/standard-model.hh>

namespace eos
{
    template <typename Tag> class WilsonScanComponent;

    template <> class WilsonScanComponent<components::DeltaB1> :
        public virtual ModelComponent<components::DeltaB1>
    {
        private:
            /* lepton dependence */
            bool _lepton_dep;

            /* QCD parameters */
            UsedParameter _alpha_s_Z__deltab1;
            UsedParameter _mu_b__deltab1;

            /* Masses */
            UsedParameter _m_Z__deltab1;

            /* Renormalization scale */
            UsedParameter _mu__deltab1;

            /* b->s Wilson coefficients */
            UsedParameter _c1;
            UsedParameter _c2;
            UsedParameter _c3;
            UsedParameter _c4;
            UsedParameter _c5;
            UsedParameter _c6;
            Parameter _abs_c7, _arg_c7;
            Parameter _re_c7, _im_c7;
            std::function<complex<double> ()> _c7;
            UsedParameter _c8;
            Parameter _abs_c9, _arg_c9;
            Parameter _re_c9, _im_c9;
            std::function<complex<double> ()> _c9;
            Parameter _abs_c10, _arg_c10;
            Parameter _re_c10, _im_c10;
            std::function<complex<double> ()> _c10;
            Parameter _abs_c7prime, _arg_c7prime;
            Parameter _re_c7prime, _im_c7prime;
            std::function<complex<double> ()> _c7prime;
            UsedParameter _c8prime;
            Parameter _abs_c9prime, _arg_c9prime;
            Parameter _re_c9prime, _im_c9prime;
            std::function<complex<double> ()> _c9prime;
            Parameter _abs_c10prime, _arg_c10prime;
            Parameter _re_c10prime, _im_c10prime;
            std::function<complex<double> ()> _c10prime;
            Parameter _abs_cS, _arg_cS;
            Parameter _re_cS, _im_cS;
            std::function<complex<double> ()> _cS;
            Parameter _abs_cSprime, _arg_cSprime;
            Parameter _re_cSprime, _im_cSprime;
            std::function<complex<double> ()> _cSprime;
            Parameter _abs_cP, _arg_cP;
            Parameter _re_cP, _im_cP;
            std::function<complex<double> ()> _cP;
            Parameter _abs_cPprime, _arg_cPprime;
            Parameter _re_cPprime, _im_cPprime;
            std::function<complex<double> ()> _cPprime;
            Parameter _abs_cT, _arg_cT;
            Parameter _re_cT, _im_cT;
            std::function<complex<double> ()> _cT;
            Parameter _abs_cT5, _arg_cT5;
            Parameter _re_cT5, _im_cT5;
            std::function<complex<double> ()> _cT5;

            /* lepton dependent wilson coefficients */
            Parameter _abs_c9_e, _arg_c9_e;
            Parameter _re_c9_e, _im_c9_e;
            std::function<complex<double> ()> _c9_e;
            Parameter _abs_c10_e, _arg_c10_e;
            Parameter _re_c10_e, _im_c10_e;
            std::function<complex<double> ()> _c10_e;
            Parameter _abs_c9prime_e, _arg_c9prime_e;
            Parameter _re_c9prime_e, _im_c9prime_e;
            std::function<complex<double> ()> _c9prime_e;
            Parameter _abs_c10prime_e, _arg_c10prime_e;
            Parameter _re_c10prime_e, _im_c10prime_e;
            std::function<complex<double> ()> _c10prime_e;
            Parameter _abs_cS_e, _arg_cS_e;
            Parameter _re_cS_e, _im_cS_e;
            std::function<complex<double> ()> _cS_e;
            Parameter _abs_cSprime_e, _arg_cSprime_e;
            Parameter _re_cSprime_e, _im_cSprime_e;
            std::function<complex<double> ()> _cSprime_e;
            Parameter _abs_cP_e, _arg_cP_e;
            Parameter _re_cP_e, _im_cP_e;
            std::function<complex<double> ()> _cP_e;
            Parameter _abs_cPprime_e, _arg_cPprime_e;
            Parameter _re_cPprime_e, _im_cPprime_e;
            std::function<complex<double> ()> _cPprime_e;
            Parameter _abs_cT_e, _arg_cT_e;
            Parameter _re_cT_e, _im_cT_e;
            std::function<complex<double> ()> _cT_e;
            Parameter _abs_cT5_e, _arg_cT5_e;
            Parameter _re_cT5_e, _im_cT5_e;
            std::function<complex<double> ()> _cT5_e;

            Parameter _abs_c9_mu, _arg_c9_mu;
            Parameter _re_c9_mu, _im_c9_mu;
            std::function<complex<double> ()> _c9_mu;
            Parameter _abs_c10_mu, _arg_c10_mu;
            Parameter _re_c10_mu, _im_c10_mu;
            std::function<complex<double> ()> _c10_mu;
            Parameter _abs_c9prime_mu, _arg_c9prime_mu;
            Parameter _re_c9prime_mu, _im_c9prime_mu;
            std::function<complex<double> ()> _c9prime_mu;
            Parameter _abs_c10prime_mu, _arg_c10prime_mu;
            Parameter _re_c10prime_mu, _im_c10prime_mu;
            std::function<complex<double> ()> _c10prime_mu;
            Parameter _abs_cS_mu, _arg_cS_mu;
            Parameter _re_cS_mu, _im_cS_mu;
            std::function<complex<double> ()> _cS_mu;
            Parameter _abs_cSprime_mu, _arg_cSprime_mu;
            Parameter _re_cSprime_mu, _im_cSprime_mu;
            std::function<complex<double> ()> _cSprime_mu;
            Parameter _abs_cP_mu, _arg_cP_mu;
            Parameter _re_cP_mu, _im_cP_mu;
            std::function<complex<double> ()> _cP_mu;
            Parameter _abs_cPprime_mu, _arg_cPprime_mu;
            Parameter _re_cPprime_mu, _im_cPprime_mu;
            std::function<complex<double> ()> _cPprime_mu;
            Parameter _abs_cT_mu, _arg_cT_mu;
            Parameter _re_cT_mu, _im_cT_mu;
            std::function<complex<double> ()> _cT_mu;
            Parameter _abs_cT5_mu, _arg_cT5_mu;
            Parameter _re_cT5_mu, _im_cT5_mu;
            std::function<complex<double> ()> _cT5_mu;

            Parameter _abs_c9_tau, _arg_c9_tau;
            Parameter _re_c9_tau, _im_c9_tau;
            std::function<complex<double> ()> _c9_tau;
            Parameter _abs_c10_tau, _arg_c10_tau;
            Parameter _re_c10_tau, _im_c10_tau;
            std::function<complex<double> ()> _c10_tau;
            Parameter _abs_c9prime_tau, _arg_c9prime_tau;
            Parameter _re_c9prime_tau, _im_c9prime_tau;
            std::function<complex<double> ()> _c9prime_tau;
            Parameter _abs_c10prime_tau, _arg_c10prime_tau;
            Parameter _re_c10prime_tau, _im_c10prime_tau;
            std::function<complex<double> ()> _c10prime_tau;
            Parameter _abs_cS_tau, _arg_cS_tau;
            Parameter _re_cS_tau, _im_cS_tau;
            std::function<complex<double> ()> _cS_tau;
            Parameter _abs_cSprime_tau, _arg_cSprime_tau;
            Parameter _re_cSprime_tau, _im_cSprime_tau;
            std::function<complex<double> ()> _cSprime_tau;
            Parameter _abs_cP_tau, _arg_cP_tau;
            Parameter _re_cP_tau, _im_cP_tau;
            std::function<complex<double> ()> _cP_tau;
            Parameter _abs_cPprime_tau, _arg_cPprime_tau;
            Parameter _re_cPprime_tau, _im_cPprime_tau;
            std::function<complex<double> ()> _cPprime_tau;
            Parameter _abs_cT_tau, _arg_cT_tau;
            Parameter _re_cT_tau, _im_cT_tau;
            std::function<complex<double> ()> _cT_tau;
            Parameter _abs_cT5_tau, _arg_cT5_tau;
            Parameter _re_cT5_tau, _im_cT5_tau;
            std::function<complex<double> ()> _cT5_tau;

        public:
            WilsonScanComponent(const Parameters &, const Options &, ParameterUser &);

            /* b->s Wilson coefficients */
            virtual WilsonCoefficients<BToS> wilson_coefficients_b_to_s(const std::string & lepton, const bool & cp_conjugate) const;
    };

    template <> class WilsonScanComponent<components::DeltaB2> :
        public virtual ModelComponent<components::DeltaB2>
    {
        private:
            Parameter _abs_c_deltab2, _arg_c_deltab2;
            Parameter _re_c_deltab2, _im_c_deltab2;
            std::function<complex<double> ()> _c_deltab2;

        public:
            WilsonScanComponent(const Parameters &, const Options &, ParameterUser &);

            virtual WilsonCoefficients<DeltaB2> wilson_coefficients_deltab2(const std::string & quark, const bool & cp_conjugate) const;
    };

    class WilsonScanModel :
        public Model,
        public SMComponent<components::CKM>,
        public SMComponent<components::QCD>,
        public WilsonScanComponent<components::DeltaB1>,
        public WilsonScanComponent<components::DeltaB2>
    {
        public:
            WilsonScanModel(const Parameters &, const Options &);
            virtual ~WilsonScanModel();

            static std::shared_ptr<Model> make(const Parameters &, const Options &);
    };
}

#endif
